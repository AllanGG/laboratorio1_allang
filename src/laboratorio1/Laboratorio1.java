package laboratorio1;
import java.util.Date;
import java.util.Scanner;


public class Laboratorio1 {
    
    public static void main(String[] args) {
    menu();
    }
    
    
    public static void menu(){
        String datos[][] = new String [6][5];
        
        for(int c1=0; c1<=5; c1=c1+1){
            for(int c2=0; c2<=4; c2=c2+1){
                datos[c1][c2]="0";     
            }
        }
        
        boolean ciclo = true;
        
        while (ciclo){
            Scanner scn = new Scanner (System.in);
            System.out.println(" <<<<< Bienvenido>>>>>\n Seleccione una opción: ");
            System.out.println(" 1. Jugar\n 2. Tabla de Posiciones\n 3. Reporte\n 4. Salir\n Opcion:");
            int opcion = scn.nextInt();
            switch (opcion){
                case 1:
                    jugar(datos);
                    break;
                case 2:
                    tabla(datos);
                    break;
                case 3:
                    reporte(datos);
                    break;
                case 4:
                    ciclo = false;
                    break;
            
            }
        }   
    }
    
    public static void jugar(String datos[][]){
        Scanner scn = new Scanner (System.in);
        if(datos[0][0] == "0"){
            System.out.println("Ingrese los nombres de los equipos que van a jugar:");
            int c = 0;
            while(c<=5){
                String equipo;
                equipo = scn.nextLine();
                datos[c][0] = equipo;
                c = c+1;
            }
        }
        
        int[] tempo = new int[6];
        int parejas[][] = new int [2][3];
        tempo[0] = 1;
        tempo[1] = 2;
        tempo[2] = 3;
        tempo[3] = 4;
        tempo[4] = 5;
        tempo[5] = 6;
        
        for(int c1=0; c1<=1; c1=c1+1){
            for(int c2=0; c2<=2; c2=c2+1){
                boolean i= true;
                while(i){
                    int num = (int) (Math.random() * 6);
                    if(tempo[num] != 0){
                        parejas[c1][c2]= tempo[num];
                        tempo[num]=0;
                        i= false;
                        
                        
                    }
                }     
            }
        }
        
        int valor1= parejas[0][0];
        int valor2= parejas[0][1];
        int valor3= parejas[0][2];
        int valor4= parejas[1][0];
        int valor5= parejas[1][1];
        int valor6= parejas[1][2];
       
        System.out.println("Primer partido:\n"+datos[valor1-1][0]+" vs "+datos[valor2-1][0]);
        System.out.println("Segundo partido:\n"+datos[valor3-1][0]+" vs "+datos[valor4-1][0]);
        System.out.println("Tercer partido:\n"+datos[valor5-1][0]+" vs "+datos[valor6-1][0]);
        
        
        int puntos1;
        int puntos2;
        int probi;
        
        System.out.println("\nNombre de los equipos: "+datos[valor1-1][0]+" vs "+datos[valor2-1][0]);
        int equi1 = (int) (Math.random() * 6);
        int equi2 = (int) (Math.random() * 6);
        System.out.println("Marcador:"+datos[valor1-1][0]+" "+equi1+" vs "+equi2+" "+datos[valor2-1][0]);
        
        probi=Integer.parseInt(datos[valor1-1][1])+equi1;
        datos[valor1-1][1]=probi+"";
        probi=Integer.parseInt(datos[valor1-1][2])+equi2;
        datos[valor1-1][2]=probi+"";
        
        probi=Integer.parseInt(datos[valor2-1][1])+equi2;
        datos[valor2-1][1]=probi+"";
        probi=Integer.parseInt(datos[valor2-1][2])+equi1;
        datos[valor2-1][2]=probi+"";
        
        if(equi1>equi2){
            puntos1=3;
            puntos2=0;
            
        }else{
            puntos2=3;
            puntos1=0;
        }
        if(equi1==equi2){
            System.out.println("Ocurrió un empate, vas a disparar 5 penales con el equipo("+datos[valor1-1][0]+"), si logras acertar 3 goles, ganan el partido:");
            int goles= 0;
            int cantidad=0;
            while((goles<3)&&(cantidad<5)){
                System.out.println("Del 1 al 6 en que casilla desea disparar?");
                int disparo = scn.nextInt();
                int portero = (int) (Math.random() * 5+1);
                if(disparo==portero){
                    System.out.println("FALLASTE");
                }else{
                    System.out.println("GOOOOOOOOL");
                    goles=goles+1;
                }
                cantidad= cantidad+1;
            }
            if(goles==3){
                System.out.println("\nGanadores: "+datos[valor1-1][0]);
                puntos1=1;
                puntos2=0;
            }else{
                System.out.println("\nGanadores: "+datos[valor2-1][0]);
                puntos2=1;
                puntos1=0;
            }
        }
        probi= Integer.parseInt(datos[valor1-1][3]) + puntos1;
        datos[valor1-1][3] = probi+"";
        probi= Integer.parseInt(datos[valor2-1][3]) + puntos2;
        datos[valor2-1][3] = probi+"";
        System.out.println("Puntos:"+datos[valor1-1][0]+" "+puntos1+" vs "+puntos2+" "+datos[valor2-1][0]);
        
        java.util.Date fecha = new Date();
        System.out.println("Fecha: "+fecha);
        
        if(puntos1==3){
            probi= Integer.parseInt(datos[valor1-1][4]) + 1;
            datos[valor1-1][4] = probi+"";
        }else{
            probi= Integer.parseInt(datos[valor2-1][4]) + 1;
            datos[valor2-1][4] = probi+"";
        }
        
        
        
        
        
        
        
        
        
        System.out.println("\nNombre de los equipos: "+datos[valor3-1][0]+" vs "+datos[valor4-1][0]);
        equi1 = (int) (Math.random() * 6);
        equi2 = (int) (Math.random() * 6);
        
        System.out.println("Marcador:"+datos[valor3-1][0]+" "+equi1+" vs "+equi2+" "+datos[valor4-1][0]);
        
        
        probi=Integer.parseInt(datos[valor3-1][1])+equi1;
        datos[valor3-1][1]=probi+"";
        probi=Integer.parseInt(datos[valor3-1][2])+equi2;
        datos[valor3-1][2]=probi+"";
        
        probi=Integer.parseInt(datos[valor4-1][1])+equi2;
        datos[valor4-1][1]=probi+"";
        probi=Integer.parseInt(datos[valor4-1][2])+equi1;
        datos[valor4-1][2]=probi+"";
        
        
        if(equi1>equi2){
            puntos1=3;
            puntos2=0;
        }else{
            puntos2=3;
            puntos1=0;
        }
        if(equi1==equi2){
            System.out.println("Ocurrió un empate, vas a disparar 5 penales con el equipo("+datos[valor3-1][0]+"), si logras acertar 3 goles, ganan el partido:");
            int goles= 0;
            int cantidad=0;
            while((goles<3)&&(cantidad<5)){
                System.out.println("Del 1 al 6 en que casilla desea disparar?");
                int disparo = scn.nextInt();
                int portero = (int) (Math.random() * 5+1);
                if(disparo==portero){
                    System.out.println("FALLASTE");
                }else{
                    System.out.println("GOOOOOOOOL");
                    goles=goles+1;
                }
                cantidad= cantidad+1;
            }
            if(goles==3){
                System.out.println("\nGanadores: "+datos[valor3-1][0]);
                puntos1=1;
                puntos2=0;
            }else{
                System.out.println("\nGanadores: "+datos[valor4-1][0]);
                puntos2=1;
                puntos1=0;
            }
        }
        probi= Integer.parseInt(datos[valor3-1][3]) + puntos1;
        datos[valor3-1][3] = probi+"";
        probi= Integer.parseInt(datos[valor4-1][3]) + puntos2;
        datos[valor4-1][3] = probi+"";
        System.out.println("Puntos:"+datos[valor3-1][0]+" "+puntos1+" vs "+puntos2+" "+datos[valor4-1][0]);
        System.out.println("Fecha: "+fecha);
        
        if(puntos1==3){
            probi= Integer.parseInt(datos[valor3-1][4]) + 1;
            datos[valor3-1][4] = probi+"";
        }else{
            probi= Integer.parseInt(datos[valor4-1][4]) + 1;
            datos[valor4-1][4] = probi+"";
        }
        
        
        
        
        
        
        
        
        System.out.println("\nNombre de los equipos: "+datos[valor5-1][0]+" vs "+datos[valor6-1][0]);
        equi1 = (int) (Math.random() * 6);
        equi2 = (int) (Math.random() * 6);
        
        System.out.println("Marcador:"+datos[valor5-1][0]+" "+equi1+" vs "+equi2+" "+datos[valor6-1][0]);
        
        
        probi=Integer.parseInt(datos[valor5-1][1])+equi1;
        datos[valor5-1][1]=probi+"";
        probi=Integer.parseInt(datos[valor5-1][2])+equi2;
        datos[valor5-1][2]=probi+"";
        
        probi=Integer.parseInt(datos[valor6-1][1])+equi2;
        datos[valor6-1][1]=probi+"";
        probi=Integer.parseInt(datos[valor6-1][2])+equi1;
        datos[valor6-1][2]=probi+"";
        
        
        if(equi1>equi2){
            puntos1=3;
            puntos2=0;
        }else{
            puntos2=3;
            puntos1=0;
        }
        if(equi1==equi2){
            System.out.println("Ocurrió un empate, vas a disparar 5 penales con el equipo("+datos[valor5-1][0]+"), si logras acertar 3 goles, ganan el partido:");
            int goles= 0;
            int cantidad=0;
            while((goles<3)&&(cantidad<5)){
                System.out.println("Del 1 al 6 en que casilla desea disparar?");
                int disparo = scn.nextInt();
                int portero = (int) (Math.random() * 5+1);
                if(disparo==portero){
                    System.out.println("FALLASTE");
                }else{
                    System.out.println("GOOOOOOOOL");
                    goles=goles+1;
                }
                cantidad= cantidad+1;
            }
            if(goles==3){
                System.out.println("\nGanadores: "+datos[valor5-1][0]);
                puntos1=1;
                puntos2=0;
            }else{
                System.out.println("\nGanadores: "+datos[valor6-1][0]);
                puntos2=1;
                puntos1=0;
            }
        }
        probi= Integer.parseInt(datos[valor5-1][3]) + puntos1;
        datos[valor5-1][3] = probi+"";
        probi= Integer.parseInt(datos[valor6-1][3]) + puntos2;
        datos[valor6-1][3] = probi+"";
        System.out.println("Puntos:"+datos[valor5-1][0]+" "+puntos1+" vs "+puntos2+" "+datos[valor6-1][0]);
        System.out.println("Fecha: "+fecha); 
        
        if(puntos1==3){
            probi= Integer.parseInt(datos[valor5-1][4]) + 1;
            datos[valor5-1][4] = probi+"";
        }else{
            probi= Integer.parseInt(datos[valor6-1][4]) + 1;
            datos[valor6-1][4] = probi+"";
        }
        
    }
    
    public static void tabla(String datos[][]){        
       System.out.println("Equipos      GF      GC     PTS       V");
       for(int c=0; c<=5; c=c+1){
           System.out.println(datos[c][0]+"       "+datos[c][1]+"     "+datos[c][2]+"     "+datos[c][3]+"     "+datos[c][4]);
       }
       
       
    }
    
    public static void reporte(String datos[][]){
        for(int c=0; c<=5; c=c+1){
            System.out.println("Equipos "+datos[c][0]+": "+datos[c][4]+" ganados.");
        }
    }
}

